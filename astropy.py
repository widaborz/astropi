from sense_hat import SenseHat
#importo la libreria time
import time

sense = SenseHat()
sense.set_rotation(270)

#messaggio a schermo
sense.show_message("My name should be Marie Curie") 
sense.show_message("My name should be Marie Curie", scroll_speed = 0.04) #scroll_speed varia tra 0 e 1 e cambia la velocità di scorrimento
sense.show_message("My name should be Marie Curie", text_colour = (0, 0, 0), back_colour = (255, 255, 255)) #cambia i colori del testo e dello sfondo

#assegno i colori a delle variabili
black = (0, 0, 0)
white = (255, 255, 255)
sense.show_message("My name should be Marie Curie", text_colour = white, back_colour = black) #e li riutilizzo nel mio codive


#lettura umidita
humidity_full = sense.get_humidity() #leggo il valore dell'umidità 
humidity = round(humidity_full,1); #arrotondo il valore ad una cifra decimale

sense.show_message("The humidity in the room is: ", text_colour = white, back_colour = black, scroll_speed = 0.05)
sense.show_message(str(humidity) + "%", text_colour = white, back_colour = black, scroll_speed = 0.1) #humidity viene trasformata in stringa con al funzione str(), poichè la funzione show_message accetta solo stringhe e non numeri. il + mi permette di contatenare le stringhe


#espressione condizionale
humidity_desired = 55

if humidity > humidity_desired: #se il valore dell'umidità è maggiore dell'umidita desiderata
  text_colour = (0,182,214) #cambia il valore del colore di testo con b
else:
  text_colour = (255, 221, 0) #altrimenti imposta il colore del testo con y 

sense.show_message("The humidity in the room is: ", text_colour = text_colour, back_colour = black, scroll_speed = 0.05)
sense.show_message(str(humidity) + "%", text_colour = text_colour, back_colour = black, scroll_speed = 0.1)


#CREAZIONI IMMAGINI

#creo prima i miei colori. Conviene utilizzare una sola lettera nel nome della variabile per lavorare meglio sui pixel

w = (255, 255, 255) #white
b = (0,182,214) #blu
a = (101,210,253) #azure
p = (238, 0, 255) #purple
g = (1, 121, 21) #green
y = (255, 221, 0) #yellow

#creo la mia immagine utilizzando una matrice 8x8. Ogni elemento è un pixel dello schermo a cui posso assegnare un colore. 

my_image = [b, b, b, b, b, b, b, w,
            p, a, b, b, b, w, a, b,
            p, b, b, b, p, b, b, b,
            p, p, b, p, p, p, b, w,
            p, p, p, p, p, p, p, a,
            p, p, b, p, p, p, b, b,
            p, b, b, b, p, b, b, b,
            b, a, a, b, b, b, a, b]

#stampo l'immagine sullo schermo
sense.set_pixels(my_image)
#metto in pausa per 3 secondi
time.sleep(3)


#IMMAGINE ANIMATA

#creo la prima immagine
dry = [a, a, a, a, a, a, a, a,
       b, b, b, b, g, g, b, b,
       a, a, g, a, g, g, a, a,
       b, b, g, b, g, g, b, g,
       a, a, g, g, g, g, g, g,
       b, b, b, b, g, g, b, b,
       y, y, y, y, g, g, y, y,
       y, y, y, y, y, y, y, y]

#creo la seconda immagine
dry2 = [b, b, b, b, b, b, b, b,
       a, a, a, a, g, g, a, a,
       b, b, g, b, g, g, b, b,
       a, a, g, a, g, g, a, g,
       b, b, g, g, g, g, g, g,
       b, b, b, b, g, g, b, b,
       y, y, y, y, g, g, y, y,
       y, y, y, y, y, y, y, y]

#creo un ciclo iterativo, cicla per 7 vole le istruzioni contenute 
for x in range(7):
    sense.set_pixels(dry)
    time.sleep(0.4)
    sense.set_pixels(dry2)
    time.sleep(0.4)       
