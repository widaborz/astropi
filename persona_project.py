from sense_hat import SenseHat
import time
sense = SenseHat()
sense.set_rotation(270)

#my colour
black = (0, 0, 0)
white = (255, 255, 255)
w = (255, 255, 255)
blu = (0, 170, 255)

b = (0,182,214)
a = (101,210,253)
p = (238, 0, 255)

g = (1, 121, 21)
y = (255, 221, 0)

#my draw
wet = [b, b, b, b, b, b, b, w,
       p, a, b, b, b, w, a, b,
       p, b, b, b, p, b, b, b,
       p, p, b, p, p, p, b, w,
       p, p, p, p, p, p, p, a,
       p, p, b, p, p, p, b, b,
       p, b, b, b, p, b, b, b,
       b, a, a, b, b, b, a, b]

wet2 = [b, b, b, b, b, w, b, b,
       p, a, b, b, b, b, a, w,
       p, b, b, b, p, w, b, b,
       p, p, b, p, p, p, w, b,
       p, p, p, p, p, p, p, a,
       p, p, b, p, p, p, b, b,
       p, b, b, b, p, b, b, b,
       b, a, a, b, b, b, a, b]

       
dry = [a, a, a, a, a, a, a, a,
       b, b, b, b, g, g, b, b,
       a, a, g, a, g, g, a, a,
       b, b, g, b, g, g, b, g,
       a, a, g, g, g, g, g, g,
       b, b, b, b, g, g, b, b,
       y, y, y, y, g, g, y, y,
       y, y, y, y, y, y, y, y]

dry2 = [b, b, b, b, b, b, b, b,
       a, a, a, a, g, g, a, a,
       b, b, g, b, g, g, b, b,
       a, a, g, a, g, g, a, g,
       b, b, g, g, g, g, g, g,
       b, b, b, b, g, g, b, b,
       y, y, y, y, g, g, y, y,
       y, y, y, y, y, y, y, y]

#my setting

humidity = round(sense.get_humidity(),1);
humidity_desired = 65

#my program 

if humidity > humidity_desired:
  text_colour = b
else:
  text_colour = y
  
sense.show_message("The humidity in the room is: ", text_colour = text_colour, back_colour = black, scroll_speed = 0.05)
sense.show_message(str(humidity) + "%", text_colour = text_colour, back_colour = black, scroll_speed = 0.1)

if humidity > humidity_desired:
  for x in range(7):
    sense.set_pixels(wet)
    time.sleep(0.4)
    sense.set_pixels(wet2)
    time.sleep(0.4)
else:
  for x in range(7):
    sense.set_pixels(dry)
    time.sleep(0.4)
    sense.set_pixels(dry2)
    time.sleep(0.4)

sense.show_message("My name should be Marie Curie", scroll_speed = 0.04)
sense.show_message("Ciao da Matteo", scroll_speed = 0.02)